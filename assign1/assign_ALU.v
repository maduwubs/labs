`timescale 1ns / 1ps

// counting from the left of the board
// first six switches = input 1
// next six = input 2
// 

module assign_ALU(
        input [5:0] x,y,
        input [2:0] fxn,
        //input sel,        // no need for sel here as the function decides what sel would be
        output reg overflow,
        output reg c_out,
        output reg [5:0] sum
    );
    
    wire c_out_010, c_out_011, c_out_110, c_out_111;                                           
    wire overflow_010, overflow_011, overflow_110,  overflow_111;                        
    wire [5:0] sum3, sum4, sum5, sum6, comp_result, xnor_result;    
    
                                                                                           
// creating an instance of the modules for the functions   
// this uses the six bit adder module from the six bit adder file                               
// input1 - input2 - sel - overflow - c_out - sum                                          
six_bit_adder fxn_010 (0, x, 1, overflow_010, c_out_010, sum3);   // -X             
six_bit_adder fxn_011 (0, y, 1, overflow_011, c_out_011, sum4);   // -Y   
six_bit_comp fxn_100 (x, y, comp_result);   
xNOR fxn_101 (x, y, xnor_result);                      // 
six_bit_adder fxn_110 (x, y, 0, overflow_110, c_out_110, sum5);   // X + Y           
six_bit_adder fxn_111 (x, y, 1, overflow_111, c_out_111, sum6);   // X - Y    
       
    

always @ (*) begin

// To avoid latches                                                                  
overflow = 0;                                                                          
c_out = 0;  
    case (fxn)
        
        3'b000: sum = x;        // X
            
        3'b001: sum = y;        // Y
        
        3'b010: begin           // -X
            sum = sum3;
            overflow = overflow_010;
            c_out = c_out_010; 
        end
        
        3'b011: begin
            overflow = overflow_011;
            c_out = c_out_011;
            sum = sum4;     // -Y 
        end
        
        3'b110: begin
            overflow = overflow_110;
            c_out = c_out_110;
            sum = sum5;     // X+Y 
        end
        
        3'b111: begin
            overflow = overflow_111;
            c_out = c_out_111;
            sum = sum6;     // X-Y 
        end
        
        3'b100: begin
            //if(comp_result == 1)
                sum = comp_result;
        end
        
        3'b101: begin
            //if(comp_result == 1)
                sum = xnor_result;
        end
        
        
        default: sum = x;
        
        
    endcase
    
end   
    
endmodule









































