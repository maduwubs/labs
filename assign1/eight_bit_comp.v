module eight_bit_comp (x, y, result);
    
input [5:0] x, y;
output [5:0] result;


    assign result = x < y || (x==y);     //  1 if x is greater than or equal to y
    
endmodule

