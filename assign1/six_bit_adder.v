`timescale 1ns / 1ps

// counting from the left of the board
// first six switches = input 1
// next six = input 2
// 

module six_bit_adder(
        input [5:0] x,y, 
        //input [2:0] fxn,
        input sel,
        output overflow,
        output c_out,
        output [5:0] sum
    );
    
    wire [5:0] y_int, y_comp;
    assign y_comp = ~y + 1;
    assign y_int = sel?  y_comp : y;        // if the value of sel is 1, then subtraction (get 2's complement, if 0 then addition
    
    wire c1, c2, c3, c4, c5, c6;
    
    FullAdder bit0 (x[0], y_int[0], 0,  sum[0], c1);
    FullAdder bit1 (x[1], y_int[1], c1, sum[1], c2);
    FullAdder bit2 (x[2], y_int[2], c2, sum[2], c3);
    FullAdder bit3 (x[3], y_int[3], c3, sum[3], c4);
    FullAdder bit4 (x[4], y_int[4], c4, sum[4], c5);
    FullAdder bit5 (x[5], y_int[5], c5, sum[5], c6);
    
    xor g0(overflow,c5, c6);
    assign c_out = c6;
    
   
    
endmodule
