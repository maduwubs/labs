module xNOR(x, y, result);

input [5:0] x, y;
output [5:0] result;


    assign result[0] = (x[0]==y[0]);     //  1 if x is greater than or equal to y
    assign result[1] = (x[1]==y[1]);
    assign result[2] = (x[2]==y[2]);
    assign result[3] = (x[3]==y[3]);
    assign result[4] = (x[4]==y[4]);
    assign result[5] = (x[5]==y[5]);

endmodule
