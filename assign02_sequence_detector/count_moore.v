`timescale 1ns / 1ps

module counter
#(parameter N = 18)
(
    input clk, reset, y,
    input max_tick_reg,
    input [N-1:0] Q_state,   // seed
    output reg [N-1:0] num_ones,
    output reg [N-1:0] num_zeros,
    output reg [7:0] count_value
);

wire [N-1:0] first_Q = Q_state;

always @ (posedge clk) begin
    
    if(!reset)
    begin
        if (y == 1'd1) 
            count_value <= count_value +8'd1;
            
        if(Q_state[N-1] == 1'd1)
            num_ones <= num_ones + 18'd1;   // increment the number of 1s
            
        if(Q_state[N-1] == 1'd0)
            num_zeros <= num_zeros + 18'd1;             
    end
    else begin
        count_value <= 8'd0;
        num_ones <= 18'd0;
        num_zeros <= 18'd0;
    end
   
    if (max_tick_reg == 1) 
    begin
        count_value <= 8'd0;
        num_ones <= 18'd0;
        num_zeros <= 18'd0;
    end 
    
end

endmodule
