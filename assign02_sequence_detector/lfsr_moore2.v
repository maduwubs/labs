`timescale 1ns / 1ps

module moore_FSM
#(parameter N = 18)(
    input clk, reset, en,
    input x,
    output reg y
    );


// MADUWUBA SARAH 90 18 xnor 100000011110000111 11000000001
// 2 ^ 3 = 8 --- too small, so we use 2 ^ 4 = 16 to represent possible bit combos;
// minimum number of state bits: n = log2[m]

parameter        s0 = 4'd0,  s1 = 4'd1, s2 = 4'd2,
                 s3 = 4'd3,  s4 = 4'd4, s5 = 4'd5,
                 s6 = 4'd6,  s7 = 4'd7, s8 = 4'd8,
                 s9 = 4'd9, s10= 4'd10, s11 = 4'd11;
    
    
// signal declaration 
reg[3:0] curr_state, next_state;
reg[3:0] state_num;


always @ (posedge clk, posedge reset)
begin
    if(reset)
    begin
        curr_state <= s0;
        state_num <= 4'd0;
        //y <= 1'b0;
    end
    else
        curr_state <= next_state;
end

// making decisions based on the current state

always @ *      //(curr_state, x)
begin
     
    next_state = curr_state;
    y = 1'b0;
    
    case (curr_state)
        s0: begin
            if(x == 1)      next_state = s1;       // move to the state 1
            else            next_state = s0;       // else remain in this state
        end
        
        
        s1: begin
            if(x == 1)      next_state = s2;        // move to the state 2

            else            next_state = s0;       // else remain in this state
        end
        
        s2: begin
            if (x == 0)
                begin
                    state_num = state_num + 4'd1; 
                    
                    if(state_num == 4'd8)
                    begin
                            state_num = 4'd0;
                            next_state = s10;
                    end
                    else
                            next_state = s2;
                end
            else
            begin           
                            next_state = s1;       // else remain in this state
            end
        end
                
        s10: begin
            if(x == 1)      
                begin
                    next_state = 11;
                    y = 1'b1;
                end
                
            else    next_state = s0;       
        end 
        
      s11:begin
            if(x == 0)      next_state = s0;
            else            next_state = s1;
      end
            
        default:
            next_state = s0;      
    endcase
end


// Program for the output
// Add for all all the states?? y <= 1'b0
//assign y = (curr_state == s12)?  1'b1 : 1'b0;        // assign 1 to output y if the sequence is found in the LFSR

//assign yl = (state-reg==sO) || (state-reg==sl);

//always @ (posedge clk) 
//begin
    
//    if(next_state != s0)
//        state_num = 1'd0;
    
//end

    
endmodule
