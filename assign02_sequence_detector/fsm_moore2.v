`timescale 1ns / 1ps

module moore_FSM
#(parameter N = 18)(
    input clk, reset, en,
    input x,
    output reg y
    );


// MADUWUBA SARAH 90 18 xnor 100000011110000111 11000000001
// 2 ^ 3 = 8 --- too small, so we use 2 ^ 4 = 16 to represent possible bit combos;
// minimum number of state bits: n = log2[m]
parameter        s0 = 4'd0,  s1 = 4'd1, s2 = 4'd2,
                 s3 = 4'd3,  s4 = 4'd4, s5 = 4'd5,
                 s6 = 4'd6,  s7 = 4'd7, s8 = 4'd8,
                 s9 = 4'd9, s10= 4'd10, s11 = 4'd11;
    
    
// signal declaration 
reg[3:0] curr_state, next_state;
reg[3:0] state_num;


always @ (posedge clk, posedge reset)
begin
    if(reset)
    begin
        curr_state <= s0;
        state_num <= 4'd0;
        //y <= 1'b0;
    end
    else
        curr_state <= next_state;
end

// making decisions based on the current state
always @ *      //(curr_state, x)
begin
     
    next_state = curr_state;
    y = 1'b0;
    
    case (curr_state)
        s0: begin
            if(x == 1)      next_state = s1;       // move to the state 1
            else            next_state = s0;       // else remain in this state
        end
        
        
        s1: begin
            if(x == 1)      next_state = s2;        // move to the state 2

            else            next_state = s0;       // start check again
        end
        
        // As all of the cases are the same, i.e. x = 0, we give the same condition
        // to each incrementing the state each time
        s2, s3, s4, s5, s6, s7, s8, s9:             
        begin
        if (x == 0)
        begin
             next_state = next_state + 1'd1;
        end
        
        else            
            next_state = s1;
   
        end
               
        s10: begin                              
            if(x == 1)                          
                begin
                    y = 1'b1;                   // sequence found
                    next_state = 11;           // move to the state 11
                end
                
            else    next_state = s0;            // start check again
        end 
        
      s11:begin                                 
            if(x == 0)      next_state = s0;     // restart checking again (go back to first state)
            else            next_state = s1;     // start check again
      end
            
        default:
            next_state = s0;      
    endcase
end

    
endmodule
