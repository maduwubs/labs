`timescale 1ns / 1ps
// STIMULUS GENERATOR
module stim_gen #(parameter T=20)(
    output reg clk, reset, 
    output reg a, b, inc_exp, dec_exp
);

always 
begin
    clk = 1'b1;
    #(T/2);
    clk = 1'b0;
    #(T/2);
end


//Test Procedure for the counter
initial
begin
    initialise();
    carEnter(3);
    carExit(2);
    carEnter(5);
    
end


//=========================================================================
//  TASK DEFINITIONS
//=========================================================================

// Assert reset between the clock edges
// clears the counter asynchronously (irrespective of the clock)
// by generating a short reset pulse
task clr_counter_asyn();
begin
    @(posedge clk);       // wait for the falling edge
    reset = 1'b1;
    #(T/4);
    reset = 1'b0;
end
endtask


// Initialise the System Generator Values
task initialise();
begin
    a = 0;
    b = 0;
    reset = 1;
    inc_exp = 0;
    dec_exp = 0;
    clr_counter_asyn();
end
endtask

// Count up 
task carEnter(input integer C);
begin

    repeat(C) @ (posedge clk)
    begin
    
    a = 1'b0;   b = 1'b0;
    #(T);
    a = 1'b1;   b = 1'b0;       
    #(T);
    a = 1'b1;   b = 1'b1;       
    #(T);
    a = 1'b0;   b = 1'b1;       
    #(T);
    a = 1'b0;   b = 1'b0;
    inc_exp = 1'b1;
    #(T);
    inc_exp = 1'b0; 
    end
     
end
endtask



// Count up 
task carExit(input integer C);
begin

    repeat(C) @ (posedge clk)
    begin
    
    a = 1'b0;   b = 1'b0;
    #(T);
    a = 1'b0;   b = 1'b1;       
    #(T);
    a = 1'b1;   b = 1'b1;       
    #(T);
    a = 1'b1;   b = 1'b0;       
    #(T);
    a = 1'b0;   b = 1'b0;
    dec_exp = 1'b1;
    
    #(T);
    dec_exp = 1'b0; 
    end
     
end
endtask


endmodule

