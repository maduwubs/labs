`timescale 1ns / 1ps


module counter(
    input clk, reset, 
    input inc, dec,
    output reg [3:0] count
    );
    
  always @ (posedge clk) 
  begin
        
    //On reset initialise the value
    if(reset)       count <= 4'd0;
    
    else
    begin
        if(inc == 1'b1  && count < 4'd15)
            count <= count + 4'd1;
        if(dec == 1'b1  && count > 4'd0)
            count <= count - 4'd1;            
    end
   
  end
    
    
    
    
    
endmodule


