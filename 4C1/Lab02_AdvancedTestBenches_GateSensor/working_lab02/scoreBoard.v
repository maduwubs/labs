`timescale 1ns / 1ps


// MONITOR / SCOREBOARD MODULE 
module scoreBoard #(parameter N=4)(
    input wire clk, reset, 
    input wire inc_exp, dec_exp,
    input wire [N-1:0] count
);

reg [N-1:0] gold;
reg [39:0] err_msg; // 5-lettre message
integer log_file;       // print to file


initial 
begin
    //write to the text file
    log_file = $fopen("D:/4C1/Lab02_AdvTestBench_Sarah_Maduwuba_21366749/log.txt", "w");
    if (!log_file)
        $display("Cannot open log file");
        
    // else: Display the Headers for the file contents
    $display("Time     Reset     Inc_Exp    Dec_Exp     Count_Exp       Count\n");
 
end 


always @ (posedge clk, posedge reset)
begin
    
    if(reset)
        gold = 0;
    
    else if(inc_exp == 1 && gold < 4'd15)
    begin
        #20;
        gold = gold + 1;
    end
    
    else if(dec_exp == 1 && gold > 4'd0)
    begin
        #20;
        gold = gold - 1;
    end
    
    
    
    // Error Message
    if(count == gold)           err_msg = "PASS";           // result passes
 
    else                        err_msg = "ERROR";          // result fails
        
    
    $display("%5d,     %b,      %b     %b      %d       %d         %s",
            $time, reset,  inc_exp, dec_exp, gold, count, err_msg);   
        
end



endmodule