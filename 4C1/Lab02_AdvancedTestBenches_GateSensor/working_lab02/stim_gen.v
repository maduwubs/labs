`timescale 1ns / 1ps



// STIMULUS GENERATOR
module stim_gen #(parameter T=20)(
    output reg clk, reset, 
    output reg a, b, inc_exp, dec_exp
);

//clock signal
always 
begin
    clk = 1'b1;
    #(T/2);
    clk = 1'b0;
    #(T/2);
end


//Test Procedure for the counter
initial
begin
    reset <= 1'b1;
    initialise();
    reset_counter();
    #T;
    carEnter(3);
    carExit(1);
    #T;
    loadData(5'b01100, 5'b00110);
    #T;
    loadData(5'b00110, 5'b01100);
end


//=========================================================================
//  TASK DEFINITIONS
//=========================================================================

task reset_counter();
begin     // wait for the falling edge
    reset = 1'b1;
    #(T/2);
    reset = 1'b0;
    #(T/2);
end
endtask


// Initialise the System Generator Values
task initialise();
begin
    a = 0;
    b = 0;
    reset = 0;
    inc_exp = 0;
    dec_exp = 0;
end
endtask

// Count up 
task carEnter(input integer C);
begin

    repeat(C) @ (posedge clk)
    begin
    
    a = 1'b0;   b = 1'b0;
    #(T);
    a = 1'b1;   b = 1'b0;       
    #(T);
    a = 1'b1;   b = 1'b1;       
    #(T);
    a = 1'b0;   b = 1'b1;       
    #(T);
    a = 1'b0;   b = 1'b0;
    inc_exp = 1'b1;
    dec_exp = 1'b0;
    #(T);
    inc_exp = 1'b0; 
    dec_exp = 1'b0;
    end
     
end
endtask



// Count up 
task carExit(input integer C);
begin

    repeat(C) @ (posedge clk)
    begin
    
    a = 1'b0;   b = 1'b0;
    #(T);
    a = 1'b0;   b = 1'b1;       
    #(T);
    a = 1'b1;   b = 1'b1;       
    #(T);
    a = 1'b1;   b = 1'b0;       
    #(T);
    a = 1'b0;   b = 1'b0;
    dec_exp = 1'b1;
    
    #(T);
    dec_exp = 1'b0; 
    end
     
end
endtask


// Load file: allows us to automatically enter in test values 
task loadData(input reg [4:0]  a_data, b_data);
    begin: ld       //label
    
    integer i;
        
        for(i=4; i>=0; i = i-1)
        begin
            #(T);
            a <= a_data[i];
            b <= b_data[i];
        end
        
        if (a_data == 5'b01100 && b_data == 5'b00110)
        begin
            inc_exp = 1'b1;
            #(T);
            inc_exp = 1'b0;
        end
            
        else if (a_data == 5'b00110 && b_data == 5'b01100)
        begin
            dec_exp = 1'b1;
            #(T);
            dec_exp = 1'b0;
        end
     end
endtask

endmodule


