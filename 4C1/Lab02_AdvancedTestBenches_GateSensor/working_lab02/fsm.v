`timescale 1ns / 1ps

module fsm_gate_sensor(
    input clk, reset, a, b,
    output reg enter, exit
    );
    
    
    localparam[2:0]         s0 = 3'b000,   // both sensors unblocked  
                            s1 = 3'b001,   // just sensor a or just sensor blocked
                            s2 = 3'b010,   // both blocked
                            s3 = 3'b011,   // just sensor a unblocked
                            s4 = 3'b100;   // just sensor b unblocked
    
    
    reg[2:0] curr_state, next_state;
    
    always @(posedge clk, posedge reset)
    begin
        if(reset)       curr_state <= s0;           // start with both sensors unblocked
        else            curr_state <= next_state;
    end    
    
    
    // Car is entering 
    always @ *
    begin
      enter = 1'b0;
      exit = 1'b0;
      next_state = curr_state;
            
      case(curr_state)
            s0:
            begin
                if(a == 1  &&  b == 0)          next_state = s1;
                else if(a == 0 && b == 1)       next_state = s1;    // else a == 0 and b is equal to 1 (car exiting)
            end       
                
            s1: 
            begin
                if(a == 1  &&  b == 1)          next_state = s2;
            end

            s2:
            begin
                if(a == 0  &&  b == 1)          next_state = s3;  
                else if(a == 1  &&  b == 0)     next_state = s4;    
            end     
                
            s3:
            begin
                if(a == 0  &&  b == 0)          
                begin
                enter <= 1'b1;        
                next_state <= s0;   
                end
                
            end             
            
            s4:
            begin
                if(a == 0  &&  b == 0)          
                begin
                exit <= 1'b1;        
                next_state <= s0;   
                end
                   
            end             
            
            default:
                next_state = s0;
        endcase
    end
   
    
endmodule

