`timescale 1ns / 1ps

module top_tb(
);

localparam  T = 20;
wire clk, rst, a, b;
wire in, out, inc, dec;
wire [3:0] count;



fsm_gate_sensor  uut(.clk(clk), .reset(rst), .a(a), .b(b),.enter(in), .exit(out)); 
 
counter_gate_sensor cnt(.clk(clk), .reset(rst), .inc(in), .dec(out), .count(count));   
    
stim_gen #(.T(20)) gen_unit(.clk(clk), .reset(rst), .a(a), .b(b), .inc_exp(inc), .dec_exp(dec));
    
scoreBoard #(.N(4)) score_unit(.clk(clk), .reset(rst), .inc_exp(inc), .dec_exp(dec),.count(count));


endmodule
