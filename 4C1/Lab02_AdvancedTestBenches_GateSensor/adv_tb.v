`timescale 1ns / 1ps

// TOP LEVEL TEST BENCH 
// REFERENCE: FROM PAGE 201 (235) OF THE CHU BOOK


// MAKE ALL THE MODULES IN SEPERATE FILES AND THEN INSTANTIATE IN THIS ONE


module adv_tb(
    );
endmodule


// STIMULUS GENERATOR
module stim_gen #(parameter T=20)(
    output reg clk, reset, 
    output reg a, b, inc_exp, dec_exp
);

always 
begin
    clk = 1'b1;
    #(T/2);
    clk = 1'b0;
    #(T/2);
end


//Test Procedure for the counter
initial
begin
    initialise();
    countUp(2);
    countDown(3);
    countUp(3);
    
end


//=========================================================================
//  TASK DEFINITIONS
//=========================================================================

// Assert reset between the clock edges
// clears the counter asynchronously (irrespective of the clock)
// by generating a short reset pulse
task clr_counter_asyn();
begin
    @(posedge clk);         // wait for the falling edge
    reset = 1'b1;
    #(T/4);
    reset = 1'b0;
end
endtask


// Initialise the System Generator Values
task initialise();
begin
    clk = 0;
    reset = 0;
    a = 0;
    b = 0;
    inc_exp = 0;
    dec_exp = 0;
end
endtask

// Count up 
task countUp(input integer C);
begin

    @(posedge clk);
    begin
        inc_exp = 1'b1;
        dec_exp = 1'b0;
    end
    repeat(C) @ (posedge clk);
    
    // after repeating, initialise these back to 0
        inc_exp = 1'b0;
        dec_exp = 1'b0;
        
end
endtask



// Count up 
task countDown(input integer C);
begin

    @(posedge clk);
    begin
        inc_exp = 1'b0;
        dec_exp = 1'b1;
    end
    repeat(C) @ (posedge clk);
    
    // after repeating, initialise these back to 0
        inc_exp = 1'b0;
        dec_exp = 1'b0;
        
end
endtask




endmodule



// MONITOR / SCOREBOARD MODULE 
module scoreBoard #(parameter N=4)(
    input wire clk, reset, 
    input wire inc_exp, dec_exp, syn_clr,
    input wire [N-1:0] count
);


reg [N-1:0] count_old, gold;
reg inc_exp_old, dec_exp_old;
reg syn_clr_old;

initial 
    $display("Time      Inc_Exp / Dec_Exp      Count\n");


always @ (posedge clk)
begin
    syn_clr_old <= syn_clr;
    inc_exp_old <= inc_exp;
    dec_exp_old <= dec_exp;
    
    count_old <= count;
    
    // Calculate the desired gold value (expected counter output)
    if(syn_clr_old)
        gold = 0;
    
end

endmodule



// DUT Module: Carpark Counter FSM
module counter_FSM #(parameter N=4)(
    input wire clk, reset,
    input wire a, b,
    output wire [N-1:0] count
);

endmodule






