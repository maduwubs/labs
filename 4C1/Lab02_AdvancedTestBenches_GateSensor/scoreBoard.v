`timescale 1ns / 1ps


// MONITOR / SCOREBOARD MODULE 
module scoreBoard #(parameter N=4)(
    input wire clk, reset, 
    input wire inc_exp, dec_exp,
    input wire [N-1:0] count
);


reg [N-1:0] count_old, gold;
reg inc_exp_old, dec_exp_old;
reg reset_old;

reg [39:0] err_msg; // 5-lettre message
integer log_file;       // print to file


initial 
begin
    // Headers for the file contents
    $display("Time  Reset   Enter   Exit  Inc_Exp     Dec_Exp     Count_Exp    Count\n");

    //write to the text file
    log_file = $fopen("D:/4C1/Lab02_AdvTestBench_Sarah_Maduwuba_21366749/log.txt", "w");
    
    
    
end 

always @ (posedge clk)
begin
    reset_old <= reset;
    inc_exp_old <= inc_exp;
    dec_exp_old <= dec_exp;
    count_old <= count;
    
    // Calculate the desired gold value (expected counter output)
    if(reset_old)
    begin
        gold <= 0;
        count_old <= 0;
    end
    
    else        
    begin
    
        if(!inc_exp && !dec_exp)
            gold = count_old;
        
        else if(inc_exp_old)
            gold = count_old + 1;
            
        else if(dec_exp_old)
            gold = count_old - 1;
            
    end
    
    
    // Error Message
    if(count == gold)           err_msg = "Pass";           // result passes
 
    else                        err_msg = "ERROR";          // result fails
        
        
    
    $display("%5d,     %b     %b      %d       %d            %s",
            $time,  inc_exp, dec_exp, gold, count, err_msg);   
    
    
end

endmodule

