`timescale 1ns / 1ps


module counter_gate_sensor(
    input clk, reset, inc, dec,
    output reg [3:0] count_val
    );
    
  always @ (posedge clk) begin
    
    if(!reset)
    begin
        if(inc == 1'b1  && count_val != 4'd15)
            count_val <= count_val + 4'b0001;
        if(dec == 1'b1  && count_val != 4'd0)
            count_val <= count_val - 4'b0001;            
    end
    
    
    //On reset initialise the value
    if(reset)       count_val = 4'b0000;
    
    
    
  end
    
    
    
    
    
endmodule





