`timescale 1ns / 1ps

module tb_fsm_gate_sensor(
    );
    
reg clk, rst, gate_a, gate_b;
wire in, out;    
    
fsm_gate_sensor M1(.clk(clk), .reset(rst), .a(gate_a), .b(gate_b), .enter(in), .exit(out));   



// Generating a clock signal
localparam T = 10;
always #10 clk = ~clk;

initial begin
    clk = 1'b1;
    gate_a = 1'b0;       
    gate_b = 1'b0;
    rst = 1'b1;
    #(T);
    
    rst = 1'b0;
    
    
    // Enter Sequence
    gate_a = 1'b1;      gate_b = 1'b0;       
    #(T);
    gate_a = 1'b1;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b0;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b0;      gate_b = 1'b0;       
    #(T);
    
    // Exit Sequence
    gate_a = 1'b0;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b1;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b1;      gate_b = 1'b0;       
    #(T);
    gate_a = 1'b0;      gate_b = 1'b0;       
    #(T);
end 



    
endmodule
