`timescale 1ns / 1ps


module tb_general(
    );


reg clk, rst, inc, dec;
reg gate_a, gate_b;
wire [3:0] count;
wire in, out; //btn_db_a, btn_db_b;

       
// gate A: button 1      
// gate B: button 2  
// in = led 
// out =led 

// count [3:0] -- the four LEDs 


fsm_gate_sensor M1(.clk(clk), .reset(rst), .a(gate_a), .b(gate_b), .enter(in), .exit(out));   
    
counter_gate_sensor M2(.clk(clk), .reset(rst), .inc(in), .dec(out), .count_val(count));   


// Generating a clock signal
localparam T = 10;
always #10 clk = ~clk;

initial begin
    clk = 1'b1;
    rst = 1'b1;
    gate_a = 1'b0;       
    gate_b = 1'b0;
    inc = 1'b0;
    dec = 1'b0;
    #(T);
    
    rst = 1'b0;
    
    
    // Enter Sequence
    gate_a = 1'b1;      gate_b = 1'b0;       
    #(T);
    gate_a = 1'b1;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b0;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b0;      gate_b = 1'b0;       
    #(T);
    
    // Exit Sequence
    gate_a = 1'b0;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b1;      gate_b = 1'b1;       
    #(T);
    gate_a = 1'b1;      gate_b = 1'b0;       
    #(T);
    gate_a = 1'b0;      gate_b = 1'b0;       
    #(T);
end 

//debouncer M3(.clk(clk), .reset(reset), .button(gate_a), .button_db(btn_db_a));  

//debouncer M4(.clk(clk), .reset(reset), .button(gate_b), .button_db(btn_db_b));  
    
endmodule