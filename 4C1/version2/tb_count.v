`timescale 1ns / 1ps


module tb_count(
    );

reg clk, rst, inc, dec;
wire [3:0] count;

// Instantiating the module
counter_gate_sensor ct(.clk(clk), .reset(rst), .inc(inc), .dec(dec), .count_val(count));

localparam T = 20;
always
begin
    clk = 1'b1;
    #(T/2);
    
    clk = 1'b0;
    #(T/2);
end


initial begin

    inc = 1'b0;
    dec = 1'b0;
    rst = 1'b1;
    #100;
    
    rst = 1'b0; 
    inc = 1'b1;     #(T/2);
    inc = 1'b1;     #(T/2);
    inc = 1'b1;     #(T/2);
    inc = 1'b0;     #(T/2);
        
    dec = 1'b1;     #(T/2);
    dec = 1'b1;     #(T/2);
    dec = 1'b0;     #(T/2);
    
    inc = 1'b1;     #(T);
    inc = 1'b0;     #(T/2);
    
end


endmodule

