`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.03.2024 18:01:36
// Design Name: 
// Module Name: eightBitComparator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

// So far: this is just a two bit comparator: hence compares two bit numbers and 
// if x > y --- fxn = 001
// if x = y --- fxn = 010
// if x < y --- fxn = 100



module eightBitComparator(
        x, y, fxn
);
    
input [7:0] x, y;
output [2:0] fxn;


    assign fxn[0] = x > y;     //  1 if x is greater than y
    assign fxn[1] = (x==y);    // 1 if x is equal to y
    assign fxn[2] = x < y;     // 1 if x is less than y
    
endmodule


// test bench
module eq2_testbench;
   // signal declaration
   reg  [7:0] test_in0, test_in1; 
   wire  [2:0] test_out;

  eightBitComparator comp1(test_in0, test_in1, test_out);

   //  test vector generator
   initial
   begin
      // test vector 1
      test_in0 = 8'b00000000;
      test_in1 = 8'b00000000;
      # 50;
      // test vector 2
      test_in0 = 8'b01100000;
      test_in1 = 8'b00000000;
      # 50;
      // test vector 3
      test_in0 = 8'b01000000;
      test_in1 = 8'b00100000;
      # 50;
      // test vector 4
      test_in0 = 8'b11001100;
      test_in1 = 8'b00011000;
      # 50;
      // test vector 5
      test_in0 = 8'b00110000;
      test_in1 = 8'b11000000;
      # 50;
      // test vector 6
      test_in0 = 8'b10101010;
      test_in1 = 8'b11100000;
      # 50;
      // test vector 7
      test_in0 = 8'b10010000;
      test_in1 = 8'b00010000;
      # 50;
      // stop simulation
      $stop;
   end
   

endmodule






