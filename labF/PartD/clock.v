`timescale 1ns / 1ps

/*
    - Clock divdier module
    - Takes an input clock signal CCLK, a 32-bit value clkscale to determine the division ratio, and outputs
      a divided clock signal clk.

    - CCLK: input clock signal that drives the clock divider module
    - clkscale: 32-bit value that determines the divions ratio of the input clock signal
    - clk: output, divided clock signal generated based on the clkscale
*/


module clock(input CCLK, input [31:0] clkscale, output reg clk);

reg[31:0] clkq = 0;

always @ (posedge CCLK)
    begin
        clkq = clkq + 1;        // increment clock register
            if (clkq >= clkscale)
                begin
                    clk = ~clk;
                    clkq = 0;
                end
end


endmodule






