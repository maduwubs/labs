## This file is a general .xdc for the Basys3 rev B board

## Clock signal
set_property PACKAGE_PIN W5 [get_ports CCLK]							
	set_property IOSTANDARD LVCMOS33 [get_ports CCLK]
	#create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports clk]
    #create_clock -period 10.000 -name clk_p -waveform {0.000 5.000} [get_ports clk_p]
 
 
## Switches
set_property PACKAGE_PIN V17 [get_ports {reset}]					
	set_property IOSTANDARD LVCMOS33 [get_ports {reset}]
set_property PACKAGE_PIN V16 [get_ports {enable}]
       set_property IOSTANDARD LVCMOS33 [get_ports {enable}]


# Max_tick_reg
set_property PACKAGE_PIN L1 [get_ports {max_tick}]
    set_property IOSTANDARD LVCMOS33 [get_ports {max_tick}]

# LFSR Results
set_property PACKAGE_PIN N3 [get_ports {lfsr_reg[13]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[13]}]
set_property PACKAGE_PIN P3 [get_ports {lfsr_reg[12]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[12]}]
set_property PACKAGE_PIN U3 [get_ports {lfsr_reg[11]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[11]}]
set_property PACKAGE_PIN W3 [get_ports {lfsr_reg[10]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[10]}]
set_property PACKAGE_PIN V3 [get_ports {lfsr_reg[9]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[9]}]
set_property PACKAGE_PIN V13 [get_ports {lfsr_reg[8]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[8]}]
set_property PACKAGE_PIN V14 [get_ports {lfsr_reg[7]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[7]}]
set_property PACKAGE_PIN U14 [get_ports {lfsr_reg[6]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[6]}]
set_property PACKAGE_PIN U15 [get_ports {lfsr_reg[5]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[5]}]
set_property PACKAGE_PIN W18 [get_ports {lfsr_reg[4]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[4]}]
set_property PACKAGE_PIN V19 [get_ports {lfsr_reg[3]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[3]}]
set_property PACKAGE_PIN U19 [get_ports {lfsr_reg[2]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[2]}]
set_property PACKAGE_PIN E19 [get_ports {lfsr_reg[1]}]    
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[1]}]
set_property PACKAGE_PIN U16 [get_ports {lfsr_reg[0]}]
    set_property IOSTANDARD LVCMOS33 [get_ports {lfsr_reg[0]}]

